const express = require('express');
const bodyParser = require('body-parser');
const productRoutes = require('./src/routes/productRoutes');
const swaggerUi = require('swagger-ui-express');
const { swaggerSpec } = require('./src/routes/productRoutes');
const db = require('./src/models/index');
const cors = require('cors');
require('dotenv').config();

const app = express();

const PORT = process.env.PORT || 3001;

app.use(bodyParser.json());

// Middleware para habilitar CORS
app.use(cors());

app.use('/api', productRoutes);

// Sincronizar Sequelize con la base de datos
db.sequelize.sync().then(() => {
  console.log('Database connected and synchronized');
}).catch((err) => {
  console.error('Unable to connect to the database:', err);
});

// Configuración de Swagger
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
