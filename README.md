# prueba Jonathan Coronado NodeJS BACKEND
Esta API proporciona servicios para gestionar productos de la joyeria joyaGOLD. La documentación detallada se encuentra a continuación.

## Requisitos previos

Asegúrate de tener [Node.js](https://nodejs.org/) instalado en tu sistema.

## Descarga

Clona el repositorio:
git clone .......

## Instala
Instala las dependencias:
npm install

## Configuracion
Se cargó el archivo .env en el repositorio ya que es un archivo de configuración de base de datos. Esto no es una buena práctica, pero para la prueba técnica se hace.

## Ejecución
node app.js

## Swagger
Se instaló y se configuró Swagger para ejecutarlo desde el navegador y mostrar la información de los servicios. Al ejecutarlo ya podrá visualizar swagger

# Servicios disponibles

## Obtener todos los productos
GET /api/products

## Filtrar productos
GET /api/products/filter?nombre=JAC&tipo=anillo&material=Oro

## Obtener product por ID
GET /api/products/:id

## Crear un nuevo producto
POST /api/products

Envía una solicitud POST con el cuerpo JSON
{
  "nombre": "Anillo prueba JAC",
  "material": "Oro",
  "tipo": "anillo",
  "precio": "7000000",
  "color": "dorado",
  "peso": "2lb",
  "longitud": "10cm"
}

## Actualizar un producto existente
PUT /api/products/:id

Envía una solicitud PUT con el cuerpo JSON
{
  "nombre": "Anillo prueba JAC EDITADO",
  "material": "Plata",
  "tipo": "anillo",
  "precio": "7000000",
  "color": "dorado",
  "peso": "2lb",
  "longitud": "10cm"
}

## Eliminar un producto
DELETE /api/products/:id

