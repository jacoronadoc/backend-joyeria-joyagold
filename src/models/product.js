// src/models/product.js
const { DataTypes } = require('sequelize');
const { sequelize } = require('./index');

const Product = sequelize.define('product', {
  nombre: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  material: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  tipo: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  precio: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  color: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  peso: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  longitud: {
    type: DataTypes.STRING,
    allowNull: true,
  },
});

module.exports = Product;
