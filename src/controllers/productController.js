// src/controllers/productController.js
const Product = require('../models/product');
const { Op } = require('sequelize');

exports.getAllProducts = async (req, res) => {
  try {
    const { page = 1, pageSize = 5 } = req.query;
    const offset = (page - 1) * pageSize;

    const result = await Product.findAndCountAll({
      offset,
      limit: pageSize,
    });

    res.json({
      total: result.count,
      totalPages: Math.ceil(result.count / pageSize),
      currentPage: page,
      products: result.rows,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

exports.getProductsById = async (req, res) => {
  const { id } = req.params;
  try {
    const product = await Product.findByPk(id);
    if (product) {
      res.json(product);
    } else {
      res.status(404).json({ error: 'Product not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

exports.createProducts = async (req, res) => {
    try {
      const {
        nombre,
        material,
        tipo,
        precio,
        color,
        peso,
        longitud
      } = req.body;
  
      // Crear el product utilizando la instancia del modelo
      const newProduct = await Product.create({
        nombre,
        material,
        tipo,
        precio,
        color,
        peso,
        longitud
      });
  
      res.status(201).json(newProduct);
    } catch (error) {
      console.error('Error en createProduct:', error);
      res.status(500).json({ error: 'Error interno del servidor' });
    }
  };

  exports.updateProducts = async (req, res) => {
    const { id } = req.params;
    const {
      nombre,
      material,
      tipo,
      precio,
      color,
      peso,
      longitud
    } = req.body;
  
    try {
      // Buscar el product por ID
      const existingProduct = await Product.findByPk(id);
  
      if (existingProduct) {
        // Actualizar el product
        await existingProduct.update({
          nombre,
          material,
          tipo,
          precio,
          color,
          peso,
          longitud
        });
  
        res.json(existingProduct);
      } else {
        res.status(404).json({ error: 'Product not found' });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  };

exports.deleteProducts = async (req, res) => {
  const { id } = req.params;
  try {
    const product = await Product.findByPk(id);
    if (product) {
      await product.destroy();
      res.json({ message: 'Product deleted successfully' });
    } else {
      res.status(404).json({ error: 'Product not found' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

exports.filterProducts = async (req, res) => {
  const {
    nombre,
    material,
    tipo,
  } = req.query;

  const whereClause = {};

  // Validar que al menos un campo esté presente
  /*if (!nombre && !material && !tipo) {
    return res.status(400).json({ error: 'Debe proporcionar al menos un parámetro para filtrar (nombre, material o tipo)' });
  }*/

  if (nombre) {
    whereClause.nombre = { [Op.like]: `%${nombre}%` };
  }

  if (material) {
    whereClause.material = { [Op.like]: `%${material}%` };
  }

  if (tipo) {
    whereClause.tipo = { [Op.like]: `%${tipo}%` };
  }

  try {
    const products = await Product.findAll({ where: whereClause });
    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};
