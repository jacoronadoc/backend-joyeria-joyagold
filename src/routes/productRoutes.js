// src/routes/productRoutes.js
const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');


router.get('/products', productController.getAllProducts);

router.get('/products/filter', productController.filterProducts);

router.get('/products/:id', productController.getProductsById);

router.post('/products', productController.createProducts);

router.put('/products/:id', productController.updateProducts);

router.delete('/products/:id', productController.deleteProducts);



module.exports = router;
